---
title: "Hello, I am Marcel"
layout: "layouts/index.njk"
eleventyImport:
  collections: ["posts"]
---
👋 Hello, I am Marcel!
I am a guy who is interested in tech, infrastructure and programming.

I am a huge fan of free (libre) open-source software, Linux, and privacy. 

I am currently working as tech guy for a small software company called [Buntspecht & Rabe Softwaremanufaktur GmbH](https://buntspechtundrabe.de/).

const { DateTime } = require("luxon");
const lazyloadPlugin = require('eleventy-plugin-lazyload');
const dimensionsPlugin = require('@higby/eleventy-images');
const pluginRss = require("@11ty/eleventy-plugin-rss");
const CleanCSS = require("clean-css");

module.exports = function(eleventyConfig) {
	eleventyConfig.addPlugin(lazyloadPlugin, {
		alt: false, // if enabled, adds an empty alt attribute on images that don't have it
	});
	eleventyConfig.addPassthroughCopy("files");
	eleventyConfig.addPassthroughCopy({ 'files/robots.txt': '/robots.txt' });
	eleventyConfig.addPlugin(dimensionsPlugin);
	eleventyConfig.addPlugin(pluginRss);
	eleventyConfig.addFilter("readableDate", (dateObj, format, zone) => {
		// Formatting tokens for Luxon: https://moment.github.io/luxon/#/formatting?id=table-of-tokens
		return DateTime.fromJSDate(dateObj, { zone: zone || "utc" }).toFormat(format || "dd LLLL yyyy");
	});
	eleventyConfig.addFilter("cssmin", function(code) {
		return new CleanCSS({}).minify(code).styles;
	  });
};

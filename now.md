---
title: "Now"
layout: "layouts/page.njk"
---
Last Updated on 2024-01-05 by mzumquadrat

This page is a short summary of things that I am currently doing in my life.

## Personal life

Since I am leaving university I am currently in the process of finding new options in life. I will be joining the company I am currently working at full-time. I am considering moving and I plan to read at least one book each month.

## Work

Currently I am researching the topic of AI and LLMs and especially their use for our company. The goal is to create educational courses for people regarding this topic.